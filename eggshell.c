#include "shellVariables.h"
#include "sighandlers.h"
#include "validation.h"
#include "userInput.h"
#include "commands.h"
#include "linenoise.h"


int main(int argc, char **argv, char ** env) {

    char * line;
    char pipeline[MAX_LINE_LENGTH];
    pipeline[0] = '\0';

    //Initializing an array to store the names of the shell Variables
    char  shellVars[MAX_SHELL_VARS][MAX_NAME_LENGTH];

    //Getting CWD and storing it in cwd
    char cwd[MAX_VALUE_LENGTH];
    if(getcwd(cwd,MAX_VALUE_LENGTH) == NULL) {
        perror("Error in finding CWD!");
        setExitCode(&errno);
        exit(EXIT_FAILURE);
    }

    //Getting terminal name
    char terminal[MAX_VALUE_LENGTH];
    getTerminalName(terminal);


    //Calling the fucntion setenvWithChecker to  all shell variables with the final parameter meaning if their value can be overwritten or not, 1 - Can be overwritten  0 - Cannot be overwritten
    //If an error occurs the fucntion exits the program.

    //Getting the value of PATH, USER, HOME, SHELL predetermined by the system
    char * path = getenvWithChecker("PATH");
    char * user = getenvWithChecker("USER");
    char * home = getenvWithChecker("HOME");
    char * shell = getenvWithChecker("SHELL");


    //Setting all environment variables
    setenvWithChecker("PATH",path,1,&shellVars);
    setenvWithChecker("PROMPT","eggshell1.0> ",1,&shellVars);
    setenvWithChecker("CWD",cwd,1,&shellVars);
    setenvWithChecker("USER",user,1,&shellVars);
    setenvWithChecker("HOME",home,1,&shellVars);
    //SHELL is set to the value of CWD since the intial CWD is where the binary file is found
    setenvWithChecker("SHELL",cwd,1,&shellVars);
    setenvWithChecker("TERMINAL",terminal,0,&shellVars);
    setenvWithChecker("EXITCODE","",1,&shellVars);



    char get[MAX_NAME_LENGTH] = "PROMPT";
    int pipeNo = 0;
    int lastPipe = 0;
    int totalNumberOfPipes = 0;
    int cmdNo = 0;


    //Handling SIGINT signal
    if(signal(SIGINT, sigegghandler) == SIG_ERR){
        printf("Signal Handler Error!\n");
    }

    // Initializing a pointer of type int to NULL
    int * fd = NULL;


    while((line = linenoise(getenvWithChecker(get))) != NULL){
        //Checking if input is enter
        if(line[0] == '\0'){
            continue;
        }

        // Counting the number of pipes ,'|' in the user input
        for(int i=0;i<strlen(line);i++){
            if(line[i] == '|'){
                (totalNumberOfPipes)++;
            }
        }

        // Allocating a chunk of memory to store integers
        if(totalNumberOfPipes != 0){
            fd = (int *) malloc(2*totalNumberOfPipes* sizeof(int));
            //If memory allocation fails
            if (fd == NULL){
                perror("Allocation of Memory Failed!");
                break;
            }
        }

        //Invoking the parse and execute function
        parseAndExecute(line,&cmdNo,&pipeNo,&totalNumberOfPipes,fd,&lastPipe,pipeline,&shellVars);

        //Freeing the memory allocated above
        free(fd);


        // Free allocated memory
        linenoiseFree(line);
    }
    return 0;
}