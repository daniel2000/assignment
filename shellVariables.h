#ifndef SHELLVARIABLES_H_
#define SHELLVARIABLES_H_

//Including constants.h to import constants from
#include "constants.h"

//Including stdlib.h to use exit(),setenv() and getenv()
#include <stdlib.h>

//Including stdio.h to use printf() amongst others
#include <stdio.h>

//Including string.h to use strcmp() and strcpy()
#include <string.h>


// Function to set the value of an environment variable which includes error checking
void setenvWithChecker(char * name, char * value, int overwritten, char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]);

// Function to get the value of an environment variable which includes error checking
char * getenvWithChecker(char * name);

// Function to set EXITCODE
void setExitCode(int * errCode);

#endif


