#include "userInput.h"

// Function to execute the correct functions based on the inputted arguments
void parseAndExecute(char * line, int * cmdNo , int * pipeNo,int * totalNumberOfPipes, int * fd, int * lastpipe, char * pipeline, char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]) {
    int stop = 0;

    //Temporary Variables to store redirection file and symbol
    char tempFile[MAX_NAME_LENGTH];
    char tempRedir[3];

    // Variable to show if outputting to file or not
    int outputW = 0;
    int out,err,save_out,save_err;

    // Variable to check if this iteration is the last command of a piping command
    int currentIsLastCmd = 0;

    //Checking if input from the source command is not enter or that there are commands in the pipeline char array
    while((line[0] != '\0' && stop == 0) || (pipeline[0] != '\0')){
        //Handling SIGINT signal
        signal(SIGINT, sigprocesshandler);

        pipingInitialiser(line,pipeline,&currentIsLastCmd,cmdNo,pipeNo,totalNumberOfPipes,fd,lastpipe);

        int tokenIndex = 0;
        char * args[MAX_ARGS];


        //Invoking the method tokenizeInput to tokenize the user input
        tokenizeInput(line, &tokenIndex, args);


        int totalNumberOfArgs = tokenIndex;


        //Invoking the method argumentAnalyser to analyse the inputted arguments and make any changes
        argumentAnalyser(args,&outputW,&totalNumberOfArgs,tempRedir, tempFile, &out, &save_out);


        //Checking if the total number of argments if 1 and that an '=' sign is present in the link meaning it is a variable assignment
        if(totalNumberOfArgs == 1 && strchr(line, '=') != NULL) {

            //Invoking the method createShellVar()
            createShellVar(line, shellVars);

        } else if(strcmp(args[0],"exit") == 0){

            //To exit the program
            //Killing all processing having eggshell as a parent process
            //pkill -STOP -P getpid()
            execl("pkill","pkill","-STOP","-p",getpid(),NULL);
            exit(EXIT_SUCCESS);

        } else if(strcmp(args[0],"print") == 0){

            //Invoking the method inbuiltprint()
            inbuiltprint(args,pipeline,&totalNumberOfArgs,cmdNo,fd,shellVars);

        } else if(strcmp(args[0],"chdir") == 0){

            //Invoking the method inbuiltchdir()
            inbuiltchdir(args,&totalNumberOfArgs,shellVars);

        } else if (strcmp(args[0],"source") == 0){

            //Invoking the method inbuiltsource()
            inbuiltsource(args, cmdNo, pipeNo, totalNumberOfPipes, fd, lastpipe, &totalNumberOfArgs, shellVars);

        } else if (strcmp(args[0],"all") == 0){

            //Invoking the method inbuiltall()
            inbuiltall(shellVars);

        } else {
            //Invoking the method forkplusexec()
            forkplusexec(args,&outputW,pipeline,&totalNumberOfArgs, &currentIsLastCmd , fd,lastpipe,cmdNo,pipeNo, totalNumberOfPipes,tempRedir, tempFile, &out, &err, &save_out, &save_err,shellVars);
        }


        if(currentIsLastCmd == 1){
            //Resetting to default values ready for next pipe
            *cmdNo = 0;
            *pipeNo = 0;
            *lastpipe = 0;
            pipeline[0] = '\0';
            *totalNumberOfPipes = 0;
            fd = NULL;

        }

        if(outputW == 1){
            //Output redirected to file

            // Forcing a write of the stream stdout to the file
            fflush(stdout);
            //Closing the file descriptor
            close(out);


            // Creating a copy of the file descriptor oldfd using the file descriptor number specified in the second argument
            dup2(save_out, fileno(stdout));

            //Closing the file descriptors
            close(save_out);

        }

        stop = 1;


        //Makes sure every process is terminated
        while(wait(NULL)>0);
    }

}

//Funtion to create shell variables
void createShellVar(char * line, char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]){
    char *name;
    char *value;


    //Assigning the name of the variable and its value to temporary variable to be checked with the current shell variables
    name = strtok(line, "=");
    value = strtok(NULL, "=");

    //Checking if variable name is valid
    if(validVarName(name) == 0){
        perror("Invalid variable name!");

        //Setting the exit code
        setExitCode(&errno);

        //Killing the current process and asking the user for a new command
        kill(-1,0);

    }

    //If the first character of the value is a $, is means that the value is a variable
    if(value[0] == '$'){
        //Removing first character
        for(int i=0;i<(unsigned)strlen(value);i++){
            value[i] = value[i+1];
        }

        //Checking if variable name is valid
        if(validVarName(value) == 0){
            perror("Invalid variable name!");

            //Setting the exit code
            setExitCode(&errno);

            //Killing the current process and asking the user for a new command
            kill(-1,0);

        }

        //Getting value of variable after $
        char * value2 = getenvWithChecker(value);
        //Setting the value of the variable prepended by a $ to the variable called, name, using the pointer pname
        setenvWithChecker(name, value2,1,shellVars);
    } else {
        //Setting value of the of name
        setenvWithChecker(name, value,1,shellVars);
    }
}

//Function the analyse the inputted arguments and make changes or textual replacement of variables
void argumentAnalyser(char * args[MAX_ARGS], int * outputW,int * totalNumberOfArgs,char * tempRedir, char * tempFile, int * out, int * save_out) {
    for(int i =1;i< *totalNumberOfArgs;i++){
        if(args[i][0] == '$'){
            //Removing first character
            for(int x=0;x<(unsigned)strlen(args[i]);x++){
                args[i][x] = args[i][x+1];
            }

            char * pointer = args[i];
            //Checking if variable name is valid, hence only having A-Z a-z 0-9 _
            if(validVarName(pointer) == 0){
                //Varible name not valid
                //Check whether last character is one of the following A-Z a-z 0-9 _
                //If it isn't remove it and check again if the variable name is valid
                if(validVarChar(&args[i][strlen(args[i])-1]) == 0){
                    //Removing last character
                    char tempLastChar = args[i][strlen(args[i])-1];
                    args[i][strlen(args[i])-1] ='\0';

                    //Rechecking whether the variable is valid
                    if(validVarName(pointer) == 0){
                        perror("Invalid variable name!");

                        //Setting the exit code
                        setExitCode(&errno);

                        //Asking for user input
                        break;
                    } else {
                        //If the variable is valid, we try to get the shell variable and append again the character removed

                        //Getting value of variable
                        char value[MAX_VALUE_LENGTH];
                        strcpy(value,getenvWithChecker(args[i]));

                        //Appending the character to the value retrieved using getenv and initializing args[i] to that value
                        args[i] = strcat(value,&tempLastChar);
                        //Appending the null character to the end of the string
                        args[i][strlen(args[i])] ='\0';
                    }
                } else {
                    perror("Invalid variable name!");

                    //Setting the exit code
                    setExitCode(&errno);

                    //Asking for user input
                    break;
                }
            } else {
                //If valid variable name
                //Getting value of variable after $
                char * value = getenvWithChecker(args[i]);
                //Initializing the argument to the value of the variable
                args[i] = value;
            }
        }

        if(args[i][0] == 92 && args[i][1] == '$'){
            //Removing first character
            for(int x=0;x<(unsigned)strlen(args[i]);x++){
                args[i][x] = args[i][x+1];
            }
        }


        // Bash seems to remove first lash and leave the rest as it is
        if(args[i][0] == 92){
            //Removing first character
            for(int x=0;x<(unsigned)strlen(args[i]);x++){
                args[i][x] = args[i][x+1];
            }
        }

        if (args[i][0] == '>'){
            *outputW = 1;

            // https://stackoverflow.com/questions/8516823/redirecting-output-to-a-file-in-c
            if(args[i+1] == NULL){
                //No arguments inputted
                perror("No file name inputted!");
                //Setting the exitcode
                setExitCode(&errno);
                //Exitting the program
                exit(EXIT_FAILURE);
            }

            // When appending to a file
            if (strcmp(args[i],">>") == 0){
                // Opening a file and appending to it the text and messages
                *out = open(args[i+1],O_WRONLY|O_APPEND);
                strcpy(tempRedir,">>");
            } else {
                // When redirecting to a file, not appending
                // Opening a file to output text and error messages to the file in write only mode and enabling it to create a file if it doesnt exist
                *out = open(args[i+1],O_WRONLY|O_CREAT);
                strcpy(tempRedir,">");
            }

            strcpy(tempFile,args[i+1]);


            // Checking if file was opened correctly
            if (*out == -1) {
                perror("Failed to open file!");

                //Setting the exit code
                setExitCode(&errno);

                break;
            }


            // Creating a copy of the file descriptors, using the lowest-numbered unused file descriptor for the new
            //       descriptor.
            *save_out = dup(fileno(stdout));


            // Checking for errors
            if (dup2(*out, fileno(stdout)) == -1) {
                close(*out);
                perror("Failed to output to file");
            }

            // Clearing the last to arguments, being the '>' character and the file name and decrementing the total number of arguements by 2
            memset(args[i],'\0',strlen(args[i]));
            memset(args[i+1],'\0',strlen(args[i+1]));
            (*totalNumberOfArgs)-=2;

        }

        // Here command
        if(strcmp(args[i],"<<<") == 0 ){
            int counter = 0;
            int totalNumberofHereArgs =(*totalNumberOfArgs) - i-1;

            if(totalNumberofHereArgs == 0){
                //No arguments inputted
                perror("No HERE arguments entered!");
                //Setting the exitcode
                setExitCode(&errno);
                // Asking the user to enter another prompt
                kill(1,0);

            } else if(totalNumberofHereArgs == 1){
                //Removing first character of first word
                for(int x=0;x<(unsigned)strlen(args[i+1]);x++){
                    args[i+1][x] = args[i+1][x+1];
                }

                //Removing the last character
                args[i+1][strlen(args[i+1])-1] = '\0';
                //Moving the argument 1 'step' down
                args[i] = args[i+1];

                //Deacreasing the total number of arguments by 1 since we removed the <<<
                (*totalNumberOfArgs)--;

            } else {
                //Removing first character of first word
                for(int x=0;x<(unsigned)strlen(args[i+1]);x++){
                    args[i+1][x] = args[i+1][x+1];
                }

                //Moving the argument 1 'step' down and incrementing the counter
                args[i] = args[i+1];
                counter++;

                //Going through all arguments that are not the first and last
                while(counter < totalNumberofHereArgs-1){
                    args[i+counter] = args[i+counter+1];
                    counter++;
                }

                //Removing the last character of the last argument and moving it 1 'step' down
                args[i+counter+1][strlen(args[i+counter+1])-1] = '\0';
                args[i+counter] = args[i+counter+1];

                //Deacreasing the total number of arguments by 1 since we removed the <<<
                (*totalNumberOfArgs)--;
            }


        }
        if (args[i][0] == '<' && args[i][1] != '<'){
            //Initializing file pointer
            FILE * fp;

            //Opening file and checking for errors
            if ((fp = fopen(args[i+1], "r")) == NULL) {
                perror("Error opening file!");

                //Setting the exit code
                setExitCode(&errno);

                break;
            }


            (*totalNumberOfArgs) -= 2;
            //The number of arguments housed inside the file
            int fileargs = 0;
            //Char aray to store all the files from the file
            char allLines[20*MAX_FILE_LINE_LENGTH];
            //Char array to store each line of the file before being inputted in allLines
            char input[MAX_FILE_LINE_LENGTH];
            //Char pointer to store the tokens
            char * subtoken;



            // Getting line by line the contents of the file
            while ((fgets(input,MAX_NAME_LENGTH,fp)) != NULL) {


                //Removing the new line escape character with a null character
                for(int z = 0;z<strlen(input);z++){
                    if(input[z] == '\n'){
                        input[z] = '\0';
                    }
                }

                //Adding a space in between lines and appending the new line to the char array
                strcat(allLines," ");
                strcat(allLines,input);
            }

            /* get the first token */
            subtoken = strtok(allLines, " ");

            /* walk through other tokens */
            while( subtoken != NULL ) {
                //Setting the arguments of the program to the arguments found in the file
                args[i+fileargs] = subtoken;
                //Incrementing the number of arguments found in the file and the total number of arguemnts in the program
                fileargs++;
                (*totalNumberOfArgs)++;
                subtoken = strtok(NULL, " ");
            }


            //Closing the file and checking for errors
            if(fclose(fp) != 0 ){
                perror("Failed to close file!");

                //Setting the exit code
                setExitCode(&errno);

                break;
            }
        }
    }

}

//If a piping command is inputted, this function initialises several variables to be used later on
void pipingInitialiser(char * line, char * pipeline ,int * currentIsLastCmd,int * cmdNo, int * pipeNo, int * totalNumberOfPipes, int * fd, int * lastpipe){
    //Char pointer to store position of first pipe symbol
    char * firstpipe ;

    //If this is not the first command of a pipeline, copy the contents of pipeline to line
    if(*cmdNo != 0){
        strcpy(line, pipeline);
    }

    // Incrementing a variable pipeNo to be used while piping
    if(*cmdNo % 2 == 0 && *cmdNo != 0){
        *pipeNo = *pipeNo + 2;
    }

    //Find the position of the first '|'
    firstpipe = strchr(line,'|');

    //If this is the last pipe (ie 1 command remains), pipeline is set to NULL and another variable to show that this is the last command to be executed is set to 1
    if(*lastpipe == 1){
        pipeline[0] = '\0';
        *currentIsLastCmd = 1;


    }

    // If the character '|' exits,
    else if(firstpipe != NULL){
        // If this is the first pipe,
        if(*cmdNo == 0){
            //The pipe() function is invoked at a different position of fd for every pipe required
            for(int i = 0; i< *totalNumberOfPipes;i++){
                if (pipe(fd + i * 2) < 0) {
                    //If pipe() fails, an error message is displayed and the exitcode is set
                    perror("pipe failed!");

                    setExitCode(&errno);
                }
            }

        }

        //Copying the rest of the line after the first pipe to pipeline
        strcpy(pipeline,line+(strlen(line) - strlen(firstpipe)) +2 ); // +2 to cater for | and space
        //Setting line to the first part of the pipe only
        line[strlen(line) - strlen(firstpipe)-1] = '\0';

        // If the '|' character is not found
        if(strchr(pipeline,'|') == NULL){
            *lastpipe = 1;
        }
    }
}

//Function to tokensize user input into several arguments
void tokenizeInput(char * line, int * tokenIndex, char * args[MAX_ARGS]){
    //Tokenising the user input
    char * token;

    //Tokenising user ' ' (space) as a delimiter
    token = strtok(line, " ");

    for (*tokenIndex = 0; token != NULL && *tokenIndex < MAX_ARGS - 1; (*tokenIndex)++) {
        args[*tokenIndex] = token;
        token = strtok(NULL, " ");
    }

    // Set last token to NULL
    args[*tokenIndex] = NULL;
}
