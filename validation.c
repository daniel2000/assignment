#include "validation.h"

//Function to check if an inputted shell variable name is valid
int validVarName(char * string){
    //Checking if every single character in the passed string is a letter (a-z A-Z) a digit (0-9) or an underscore (95)
    for(int i =0;i<(unsigned)strlen(string);i++){
        if(isalpha(string[i]) == 0 && isdigit(string[i]) == 0){
            if(string[i] != 95) {
                return 0;
            }
        }
    }
    return 1;
}

//Function to check if a character is a valid character to be used in a shell variable name
int validVarChar(char * chara) {
    //Checking if the passed character is a letter (a-z A-Z) a digit (0-9) or an underscore (95)
    if(isalpha(chara[0]) == 0 && isdigit(chara[0]) == 0){
        if(chara[0] != 95) {
            return 0;
        }
    }

    return 1;
}