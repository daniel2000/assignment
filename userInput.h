#ifndef USERINPUT_H_
#define USERINPUT_H_

//Including constants.h to import constants from
#include "constants.h"

//Including commands.h to execute commands on the user input
#include "commands.h"

//Including shellVariables.h in order to be able to set and get environment variables using the custom defined functions
#include "shellVariables.h"

//Including validation.h in order to be able to validate the variable name using the custom defined functions
#include "validation.h"

//Including sighandlers.h in order to be able to handle signals
#include "sighandlers.h"

//Including sys/wait.h in order to be able to using wait()
#include <sys/wait.h>

//Including fcntl.h in order to use open()
#include <fcntl.h>

//Including errno.h in order to use errno
#include <errno.h>





// Function to execute the correct functions based on the inputted arguments
void parseAndExecute(char * line,int * cmdNo, int * pipeNo,int * totalNumberOfPipes, int * fd, int * lastpipe, char * pipeline, char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]);

//Function the analyse the inputted arguments and make changes or textual replacement of variables
void argumentAnalyser(char * args[MAX_ARGS], int * outputW,int * totalNumberOfArgs,char * tempRedir, char * tempFile, int * out, int * save_out);

//Function to create shell variables
void createShellVar(char * line, char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]);

//If a piping command is inputted, this function initialises several variables to be used later on
void pipingInitialiser(char * line, char * pipeline ,int * currentIsLastCmd,int * cmdNo, int * pipeNo, int * totalNumberOfPipes, int * fd, int * lastpipe);

//Function to tokensize user input into several arguments
void tokenizeInput(char * line, int * tokenIndex, char * args[MAX_ARGS]);

#endif