#ifndef COMMANDS_H_
#define COMMANDS_H_

//Including header folders
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <arpa/nameser.h>
#include "string.h"

//Including sys/wait.h in order to be able to using wait()
#include <sys/wait.h>

//Including fcntl.h in order to user open()
#include <fcntl.h>

//Including validation.h in order to be able to validate the variable name using the custom defined functions
#include "validation.h"

//Including shellVariables.h in order to be able to set and get environment variables using the custom defined functions
#include "shellVariables.h"

//Including shellVariables.h in order to be able to set and get environment variables using the custom defined functions
#include "userInput.h"

//To be used to display all environment variables
extern char **environ;

// Function to print to the shell specified arguments including any sheel variables
void inbuiltprint(char * args[MAX_ARGS],char * pipeline,int * totalNumberOfArgs,int * cmdNo, int * fd,char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]);

// Function to change the Current Working Directory of the sheel
void inbuiltchdir(char * args[MAX_ARGS],int * totalNumberOfArgs,char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]);

// Function to source a file and process its contents as if they were inputted in the eggshell
void inbuiltsource(char * args[MAX_ARGS], int * cmdNo, int * pipeNo, int * totalNumberOfPipes, int * fd, int * lastpipe, int * totalNumberOfArgs,char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]);

// Function to display all shell variables
void inbuiltall(char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]);

// Function to fork a child and execute a process from the child
void forkplusexec(char * args[MAX_ARGS], int * outputW,char * pipeline,int * totalNumberOfArgs, int * currentIsLastCmd ,int * fd,int * lastpipe,int * cmdNo, int * pipeNo, int * totalNumberOfPipes,char * tempRedir, char * tempFile, int * out, int * err, int * save_out, int * save_err,char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]);

// Funtion to get the terminal name that the eggshell is executing in
void getTerminalName(char * terminal);

#endif