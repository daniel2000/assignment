#ifndef SIGHANDLERS_H_
#define SIGHANDLERS_H_

#include <stdio.h>
#include <signal.h>

//Function to handle signal SIGINT, in parseandexecute while loop
void sigprocesshandler(int signo);

//Function to handle signal SIGINT, in main function
void sigegghandler(int signo);

#endif