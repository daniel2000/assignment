//Including the commands.h folder
#include "commands.h"

// Function to print to the shell specified arguments including any sheel variables
void inbuiltprint(char * args[MAX_ARGS],char * pipeline,int * totalNumberOfArgs,int * cmdNo, int * fd,char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]){
    //Checking if arguments to be printed are inputted
    if (*totalNumberOfArgs == 1) {
        perror("No arguments entered!");

        //Setting the exit code
        setExitCode(&errno);

        //Killing the current process and asking the user for a new command
        kill(-1,0);

    } else {
        for (int i = 1; i < *totalNumberOfArgs; i++) {
            printf("%s ", args[i]);
            //Flushing out the buffer, display the content immediately
            fflush(stdout);
        }
        // Skipping a line
        printf("\n");
    }

    //Setting the exit code
    setExitCode(&errno);

}

// Function to change the Current Working Directory of the sheel
void inbuiltchdir(char * args[MAX_ARGS],int * totalNumberOfArgs,char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]){
    //Checking if exactly two arugments are inputted
    if (*totalNumberOfArgs != 2) {
        perror("Invalid no of arguments!");

        //Setting the exit code
        setExitCode(&errno);

        //Killing the current process and asking the user for a new command
        kill(-1,0);

    } else {
        // Implemtation using chdir()
        if(chdir(args[1]) == 0){
            printf("CWD changed to %s\n", args[1]);

            //Getting CWD and storing it in cwd
            char cwd[MAX_VALUE_LENGTH];
            if(getcwd(cwd,MAX_VALUE_LENGTH) == NULL) {
                perror("Error in finding CWD!");

                //Setting the exit code
                setExitCode(&errno);

                //Killing the current process and asking the user for a new command
                kill(-1,0);
            }

            // Setting the value of CWD to the value brought from getcwd
            setenvWithChecker("CWD",cwd,1,shellVars);
        } else {
            perror("Failed to change CWD");

            //Setting the exit code
            setExitCode(&errno);

            //Killing the current process and asking the user for a new command
            kill(-1,0);
        }

        // -- My implmentation of chdir  --
        /*
        if(strcmp(args[1],"..") == 0) {
            //Go to parent directly
            //Initalize an array of characters to CWD
            char get[MAX_NAME_LENGTH] = "CWD";
            //Pointer to char array get
            char * pget = get;
            //Getting the value of the variable whose name is stored in get
            char * value = getenvWithChecker(&pget);

            // Storing the value in a temporary variable
            char tempVal[MAX_VALUE_LENGTH];
            strcpy(tempVal,value);

            // Intializing a variable to the length of the value
            int lenVal = strlen(value);
            // Variable to check whether '/' exits in the path or not.
            int found = 0;

            for(int i =0;i< lenVal;i++){
                //If '/' is found, remove it (by setting it to the null character) and break the for loop
                if(value[lenVal-i] == '/'){
                    value[lenVal-i] = '\0';
                    found = 1;
                    break;
                }
                //Removing the character by setting it to the null character
                value[lenVal-i] = '\0';
            }

            if(found == 0){
                perror("No parent directory!");
                // Setting the shell variable to the old value
                setenvWithChecker("CWD",tempVal,1);
            } else{
                // Setting the shell variable to the new value
                setenvWithChecker("CWD",value,1);
            }



        } else {
            //Set the shell variable CWD the value inputted as an argument
            setenvWithChecker("CWD",args[1],1);
        }
        */

        //Setting the exit code
        setExitCode(&errno);
    }
}

// Function to source a file and process its contents as if they were inputted in the eggshell
void inbuiltsource(char * args[MAX_ARGS], int * cmdNo, int * pipeNo, int * totalNumberOfPipes, int * fd, int * lastpipe, int * totalNumberOfArgs,char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]){
    //Checking if only 1 argument has been entered form the command.
    if (*totalNumberOfArgs != 2) {
        perror("Invalid no of arguments!");

        //Setting the exit code
        setExitCode(&errno);

        //Killing the current process and asking the user for a new command
        kill(-1,0);

    } else {

        //Initializing file pointer
        FILE * fp;

        //Opening file and checking for errors
        if ((fp = fopen(args[1], "r")) == NULL) {
            perror("Error opening file!");

            //Setting the exit code
            setExitCode(&errno);

            //Killing the current process and asking the user for a new command
            kill(-1,0);
        }

        char fileLine[MAX_FILE_LINE_LENGTH];

        // Getting line by line the contents of the file
        while ((fgets(fileLine,MAX_FILE_LINE_LENGTH,fp)) != NULL) {

            //Removing the new line escape character with a null character
            for(int i = 0;i<strlen(fileLine);i++){
                if(fileLine[i] == '\n'){
                    fileLine[i] = '\0';
                }
            }
            //Invoking the function parseAndExecute to parse the execute any commonds found in the file
            parseAndExecute(fileLine,cmdNo,pipeNo,totalNumberOfPipes,fd,lastpipe,"",shellVars);
        }

        //Closing the file and checking for errors
        if(fclose(fp) != 0 ){
            perror("Failed to close file!");

            //Setting the exit code
            setExitCode(&errno);

            //Killing the current process and asking the user for a new command
            kill(-1,0);
        }

        ///Setting the exit code
        setExitCode(&errno);

    }
}

// Function to display all shell variables
void inbuiltall(char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]){
    char *s  = * environ;

    // Looping through all the env variables and printing them while s is not NULL
    for(int i = 1;s;i++){
        printf("%s\n",s);
        s = *(environ+i);
    }

    // Setting the exit code
    setExitCode(&errno);

}

// Function to fork a child and execute a process from the child
void forkplusexec(char * args[MAX_ARGS], int * outputW,char * pipeline,int * totalNumberOfArgs, int * currentIsLastCmd ,int * fd,int * lastpipe,int * cmdNo, int * pipeNo, int * totalNumberOfPipes,char * tempRedir, char * tempFile, int * out, int * err, int * save_out, int * save_err,char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]){

    // fork child process
    pid_t pid = fork();
    if (pid == -1) {
        perror("fork() failed");

        //Setting the exit code
        setExitCode(&errno);

        //Killing the current process and asking the user for a new command
        kill(-1,0);

    } else if (pid == 0) {
        if(*outputW == 1){
            //Output redirected to file

            // Forcing a write of the stream stdout to the file
            fflush(stdout);
            //Closing the file descriptor
            close(*out);

            // Creating a copy of the file descriptor oldfd using the file descriptor number specified in the second argument
            dup2(*save_out, fileno(stdout));

            //Closing the file descriptor
            close(*save_out);

            // When appending to a file
            if (strcmp(tempRedir,">>") == 0){
                // Opening a file and appending to it the text and messages
                *out = open(tempFile,O_WRONLY|O_APPEND);
            } else {
                // When redirecting to a file, not appending
                // Opening a file to output text and error messages to the file in write only mode and enabling it to create a file if it doesnt exist
                *out = open(tempFile,O_WRONLY|O_CREAT);
            }


            // Checking if file was opened correctly
            if (*out == -1) {
                perror("Failed to open file!");

                //Setting the exit code
                setExitCode(&errno);

                //Killing the current process and asking the user for a new command
                kill(-1,0);
            }


            // Creating a copy of the file descriptors, using the lowest-numbered unused file descriptor for the new
            // descriptor.
            *save_out = dup(fileno(stdout));



            // Checking for errors
            if (dup2(*out, 1) == -1) {
                close(*out);

                perror("Failed to output to file");

                //Setting the exit code
                setExitCode(&errno);

                //Killing the current process and asking the user for a new command
                kill(-1,0);


            }


        }
        //For piping
        if(pipeline[0] != '\0' || *lastpipe == 1){

            //If the command is not the last command
            if(*currentIsLastCmd != 1){
                dup2(fd[(*cmdNo*2 +1)], STDOUT_FILENO);
            }

            //If not the first command
            if(*cmdNo != 0){
                dup2(fd[(*cmdNo -1 ) *2], STDIN_FILENO);
            }


            //Close all pipes
            for(int i =0;i< 2*(*totalNumberOfPipes);i++){
                close(fd[i]);
            }

        }


        // Verifying the child process contain egg shell specific variables
        if(getenv("TERMINAL") == NULL){
            perror("Shell variable TERMINAL not specified in child process!");
        }
        if(getenv("CWD") == NULL){
            perror("Shell variable CWD not specified in child process!");
        }
        if(getenv("EXITCODE") == NULL){
            perror("Shell variable CWD not specified in child process!");
        }
        if(getenv("PROMPT") == NULL){
            perror("Shell variable CWD not specified in child process!");
        }

        char * tempPath;


        //Getting the shell variable PATH and checking for errors
        tempPath = getenv("PATH");
        if(tempPath == NULL){
            perror("PATH not set!");

            //Setting the exit code and exiting the program
            setExitCode(&errno);
            exit(EXIT_FAILURE);
        }

        int tokenIndex2 = 0;
        char * token2;
        char * subPaths[MAX_VALUE_LENGTH];

        //Tokenising the PATH shell variable at every ';' storing each subpath in subPaths
        token2 = strtok(tempPath,":");

        for (tokenIndex2 = 0; token2 != NULL; tokenIndex2++) {
            subPaths[tokenIndex2] = token2;
            token2 = strtok(NULL, ":");
        }
        subPaths[tokenIndex2] = NULL;

        int notfound = 0;

        char tempSubPath[MAX_VALUE_LENGTH];

        for(int i=0;i<tokenIndex2;i++){
            //Concatinating with the individual paths found in the shell variable PATH, a / and the name of the command
            strcpy(tempSubPath,subPaths[i]);
            strcat(tempSubPath,"/");
            strcat(tempSubPath,args[0]);

            //Trying to execute the command and for each time, the exec fails, a counter incremented
            if (execv(tempSubPath, args) == -1) {
                notfound++  ;
            }

        }

        // If the above execv has gone through all sub paths and hasn't executed, execvp is called with args[0] as the file name
        if(notfound == tokenIndex2){
            if (execvp(args[0],args) == -1) {
                //If this exec also fails, an error message is outputted.
                perror("Command not found!");

                //Setting the exit code
                setExitCode(&errno);
            }

        }


    }

    //Checking if another piping command is to execute after this
    if(pipeline[0] != '\0' || *lastpipe == 1){
        (*cmdNo)++;
    }

    int status;

    //Waiting for the child to finish executing
    if (waitpid(-1,&status,0) == -1){
        perror("wait() failed");

        //Setting the exit code
        setExitCode(&errno);

        exit(EXIT_FAILURE);
    }

    if(WIFEXITED(status)){
        printf("Program exited normally!\n");
    }
    if(WIFSIGNALED(status)) {
        printf("Child terminated by a signal!\n");
    }

    //Setting the exit code
    setExitCode(&errno);
}

// Funtion to get the terminal name that the eggshell is executing in
void getTerminalName(char * terminal){
    //Getting the terminal name using piping and forking
    FILE *file = popen("tty","r");
    if (file == NULL) {
        perror("I/O stream not opened!");
        exit(EXIT_FAILURE);
    }

    //Getting the string and storing it in terminal
    if(fgets(terminal, 15,file) == NULL){
        perror("Error in finding terminal name!");
        exit(EXIT_FAILURE);
    }

    //Replacing the \n with a \0 and the end of the char array.
    terminal[strlen(terminal)-1] = '\0';

    if(pclose(file) == -1){
        perror("I/O stream not closed!");
        exit(EXIT_FAILURE);
    }
}
