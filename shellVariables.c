#include "shellVariables.h"

// Function to set the value of an environment variable which includes error checking
void setenvWithChecker(char * name, char * value, int overwritten, char (*shellVars)[MAX_SHELL_VARS][MAX_NAME_LENGTH]){
    //Checking if setenv is properly executed without any errors
    if(setenv(name, value,overwritten) == -1){
        printf("Error in setting shell variable %s!\n",name);
        exit(EXIT_FAILURE);
    }

    int noOfShellVars = atoi(shellVars[0][0]);
    int found = 0;

    //Checking if the variable is already found in the array shellVars
    for(int i=1;i<noOfShellVars+1;i++){
        if(strcmp(*(*shellVars+i),name) == 0) {
            found = 1;
        }
    }

    // If it isnt, add it to the array and change the variable storing the number of shell variables which is found at array location 0
    if(found == 0){
        char tempNo[MAX_VALUE_LENGTH];
        sprintf(tempNo, "%d", noOfShellVars+1);
        strcpy(*(*shellVars+noOfShellVars+1),name);
        strcpy(*(*shellVars),tempNo);
    }
}

// Function to get the value of an environment variable which includes error checking
char * getenvWithChecker(char * name){
    //Checking if getenv is properly executed without any errors
    if(getenv(name) == NULL){
        printf("Error in getting shell variable %s!\n",name);
        exit(EXIT_FAILURE);
    }
    else {
        return getenv(name);
    }
}

// Function to set EXITCODE
void setExitCode(int * errCode){
    char tempEr[3];
    //Converting the integer to char array
    sprintf(tempEr, "%d", *errCode);
    //Setting the environment variable EXITCODE to the new created char array
    if (setenv("EXITCODE",tempEr,1) == -1 ){
        perror("Error in setting EXITCODE!");
    }
}

