#ifndef VALIDATION_H_
#define VALIDATION_H_


//Including ctype.h for using isalpha and isdigit
#include <ctype.h>

//Including string.h for using strlen
#include <string.h>

//Function to check if an inputted shell variable name is valid
int validVarName(char * string);

//Function to check if a character is a valid character to be used in a shell variable name
int validVarChar(char * chara);

#endif