#ifndef CONSTANTS_H_
#define CONSTANTS_H_

//Defining contants to be used throughout the program and from other .h files
#define MAX_ARGS 255
#define MAX_LINE_LENGTH 500
#define MAX_SHELL_VARS 100
#define MAX_VALUE_LENGTH 50
#define MAX_NAME_LENGTH 50
#define MAX_FILE_LINE_LENGTH 100

#endif